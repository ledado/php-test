Úlohy vypracované v Symfony 2.8

Inštalácia 

1. $ composer install
2. $ php app/console doctrine:schema:update --force
3. v prehliadači otvorte priečinok web


Požiadavky

- php >= 5.3.9
- mysql
- composer https://getcomposer.org/ 


Stránky podľa zadania

1. /
2. /searchdata
3. /deletesearch