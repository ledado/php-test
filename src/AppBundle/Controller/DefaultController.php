<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Ip;
use AppBundle\Entity\Search;
use Doctrine\DBAL\Types\BigIntType;
use Doctrine\DBAL\Types\IntegerType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\Date;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('user', TextType::class)
            ->add('send', SubmitType::class, array('label' => 'Show public repos'))
            ->getForm();

        $repos = array();
        $message = '';

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $username = $form->getData()['user']; //vyhladavany text

            //request do api.github
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.github.com/users/".$username."/repos?sort=created",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    'Accept: application/json',
                    'Content-Type: application/json',
                    'User-agent: php_test'
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            //parsovanie dát z api
            if ($err) {
                echo "cURL Error #:" . $err;
            } else {

                $jsonArray = json_decode($response, true);


                if(array_key_exists("message",$jsonArray)){
                    $message = $jsonArray['message'];
                }else{
                    for($i = 0; $i < count($jsonArray); $i++){
                        $repos[$i]['name'] = $jsonArray[$i]['name'];
                        $repos[$i]['created_at'] = $jsonArray[$i]['created_at'];
                        $repos[$i]['html_url'] = $jsonArray[$i]['html_url'];

                    }

                    if($i == 0){
                        $message = '0 rows';
                    }
                }

            }

            //zaznamenávanie vyhľadávania
            $em = $this->get('doctrine')->getManager();

            $search = new Search();
            $search->setText($username);
            $search->setDate(new \DateTime('now'));

            $ip = $em->getRepository('AppBundle:Ip')
                ->findOneByIp($request->getClientIp());

            if(!$ip){
                $ip = new Ip();
                $ip->setIp($request->getClientIp());
            }

            $search->setIp($ip);

            $em->persist($search);
            $em->persist($ip);
            $em->flush();

        }

        return $this->render('AppBundle:Default:index.html.twig', array(
            'form' => $form->createView(), 'repos' => $repos, 'message' => $message
        ));
    }

    public function getSearchDataAction(){
        $em = $this->get('doctrine')->getManager();

        $searchData = $em->getRepository('AppBundle:Search')
            ->findBy(array(),array('date' => 'DESC'));

        return $this->render('AppBundle:Default:search_data.html.twig', array(
            'searchData' => $searchData
        ));
    }
    
    public function deleteSearchAction(Request $request){
        $em = $this->get('doctrine')->getManager();

        $form = $this->createFormBuilder()
            ->add('hours', 'integer')
            ->add('delete', SubmitType::class, array('label' => 'Delete older data'))
            ->getForm();

        $message = '';

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $hours = $form->getData()['hours']; //počet hodín

            if($hours < 0 ){
                $message = "Only natural numbers";
            }else{
                $searchData = $em->getRepository('AppBundle:Search')
                    ->deleteByDate(new \DateTime('-'.$hours.'hours'));
                $message = "Deleted ".$searchData." rows";
            }

        }

        return $this->render('AppBundle:Default:delete_search.html.twig', array(
            'form' => $form->createView(), 'message' => $message
        ));
    }
}
