<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ip
 */
class Ip
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $ip;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $search;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->search = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ip
     *
     * @param string $ip
     * @return Ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string 
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Add search
     *
     * @param \AppBundle\Entity\Search $search
     * @return Ip
     */
    public function addSearch(\AppBundle\Entity\Search $search)
    {
        $this->search[] = $search;

        return $this;
    }

    /**
     * Remove search
     *
     * @param \AppBundle\Entity\Search $search
     */
    public function removeSearch(\AppBundle\Entity\Search $search)
    {
        $this->search->removeElement($search);
    }

    /**
     * Get search
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSearch()
    {
        return $this->search;
    }
}
